use News
go

delete from Person
go

declare @json nvarchar(max)
    set @json = (select * from openrowset (bulk 'D:\\Sql\\NewsDB\\Sample\\JsonFiles\\persons.json',single_nclob) as j);

exec dbo.usp_insertPersons @json;
go

select * from Person
select * from PersonInfo
select * from Author
go