use News
go

delete from dbo.Photo
delete from dbo.Video
go

declare @json nvarchar(max)
    set @json = (select * from openrowset (bulk 'D:\\Sql\\NewsDB\\Sample\\JsonFiles\\media.json',single_nclob) as j);

exec dbo.usp_insertMedia @json;
go

select * from dbo.Photo
select * from dbo.Video
select * from dbo.News_Photo_Xref
select * from dbo.News_Video_Xref
select * from dbo.Author
go