use News
go

delete from Page
go

declare @json nvarchar(max)
    set @json = (select * from openrowset (bulk 'D:\\Sql\\NewsDB\\Sample\\JsonFiles\\pages.json',single_nclob) as j);

exec dbo.usp_insertPages @json;
go

select * from Page
select * from Page_News_Xref
select * from Category_Page_News_Xref
go