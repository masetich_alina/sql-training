use News
go

delete from Page
delete from News
delete from Comment
go

declare @json nvarchar(max)
    set @json = (select * from openrowset (bulk 'D:\\Sql\\NewsDB\\Sample\\JsonFiles\\news.json',single_nclob) as j);

exec dbo.usp_insertNews @json;
go

select * from News
select * from Comment
select * from Like_Comment_Person_Xref
select * from Dislike_Comment_Person_Xref
select * from News_Category_Xref
go