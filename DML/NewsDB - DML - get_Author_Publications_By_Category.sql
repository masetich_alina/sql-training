use News
go

select  p.email     as  author, 
        count(*)    as  publications, 
        c.[name]    as  category
from Author as a
    join Person                 p   on p.personId       = a.personId
    join News                   n   on n.authorId       = a.authorId
    join News_Category_Xref     nc  on nc.newsId        = n.newsId
    join Category               c   on c.categoryId     = nc.categoryId
group by p.email, c.[name] 
