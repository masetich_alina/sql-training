use News
go

delete from Category_Page_News_Xref
delete from Category
delete from Subcategory
delete from Keyword
go

declare @json nvarchar(max)
    set @json = (select * from openrowset (bulk 'D:\\Sql\\NewsDB\\Sample\\JsonFiles\\category_keyword.json',single_nclob) as j);

exec dbo.usp_insert_Category_Keyword @json;
go

select * from Category
select * from Subcategory
select * from Keyword