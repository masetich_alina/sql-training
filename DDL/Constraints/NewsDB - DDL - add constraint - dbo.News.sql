use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News__Author'
)
	alter table dbo.News
	drop constraint fk_News__Author
go

alter table dbo.News
	add constraint fk_News__Author
	foreign key (authorId) references dbo.Author(authorId)
	on delete no action
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News__MainNews'
)
	alter table dbo.News
	drop constraint fk_News__MainNews
go

alter table dbo.News
	add constraint fk_News__MainNews
	foreign key (mainNewsId) references dbo.MainNews(mainNewsId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News__Rubric'
)
	alter table dbo.News
	drop constraint fk_News__Rubric
go

alter table dbo.News
	add constraint fk_News__Rubric
	foreign key (rubricId) references dbo.Rubric(rubricId)
	on delete no action
go