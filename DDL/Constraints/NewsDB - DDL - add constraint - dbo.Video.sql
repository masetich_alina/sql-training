use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Video__Author'
)
	alter table dbo.Video
	drop constraint fk_Video__Author
go

alter table dbo.Video
	add constraint fk_Video__Author
	foreign key (authorId) references dbo.Author(authorId)
	on delete cascade
go

