use News
go

if exists
(
    select 1 
    from information_schema.referential_constraints
    where Constraint_Name ='fk_Dislike_Comment_Person_Xref__Comment'
)
	alter table dbo.Dislike_Comment_Person_Xref
	drop constraint fk_Dislike_Comment_Person_Xref__Comment
go

alter table dbo.Dislike_Comment_Person_Xref
    add constraint fk_Dislike_Comment_Person_Xref__Comment
    foreign key (commentId) references dbo.Comment(commentId)
    on delete cascade
go

if exists
(
    select 1 
    from information_schema.referential_constraints
    where Constraint_Name ='fk_Dislike_Comment_Person_Xref__Person'
)
    alter table dbo.Dislike_Comment_Person_Xref
    drop constraint fk_Dislike_Comment_Person_Xref__Person
go

alter table dbo.Dislike_Comment_Person_Xref
    add constraint fk_Dislike_Comment_Person_Xref__Person
    foreign key (personId) references dbo.Person(personId)
    on delete cascade
go