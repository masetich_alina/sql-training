use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News_Category_Xref__News'
)
	alter table dbo.News_Category_Xref
	drop constraint fk_News_Category_Xref__News
go

alter table dbo.News_Category_Xref
	add constraint fk_News_Category_Xref__News
	foreign key (newsId) references dbo.News(newsId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News_Category_Xref__Category'
)
	alter table dbo.News_Category_Xref
	drop constraint fk_News_Category_Xref__Category
go

alter table dbo.News_Category_Xref
	add constraint fk_News_Category_Xref__Category
	foreign key (categoryId) references dbo.Category(categoryId)
	on delete cascade
go