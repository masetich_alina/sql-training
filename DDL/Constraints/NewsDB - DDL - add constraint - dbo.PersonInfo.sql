use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_PersonInfo__Person'
)
	alter table dbo.PersonInfo
	drop constraint fk_PersonInfo__Person
go

alter table dbo.PersonInfo
	add constraint fk_PersonInfo__Person
	foreign key (personId) references dbo.Person(personId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_PersonInfo__PersonId__Date'
)
	alter table dbo.PersonInfo
	drop constraint ak_PersonInfo__PersonId__Date
go

alter table dbo.PersonInfo
add constraint ak_PersonInfo__PersonId__Date unique (personId, [date])
go
