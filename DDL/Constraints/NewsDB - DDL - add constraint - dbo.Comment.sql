use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Comment__Person'
)
	alter table dbo.Comment
	drop constraint fk_Comment__Person
go

alter table dbo.Comment
	add constraint fk_Comment__Person
	foreign key (personId) references dbo.Person(personId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Comment__News'
)
	alter table dbo.Comment
	drop constraint fk_Comment__News
go

alter table dbo.Comment
	add constraint fk_Comment__News
	foreign key (newsId) references dbo.News(newsId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Comment__PersonId__News__DateOfCreation'
)
	alter table dbo.Comment
	drop constraint ak_Comment__PersonId__News__DateOfCreation
go

alter table dbo.Comment
add constraint ak_Comment__PersonId__News__DateOfCreation unique (personId, newsId, dateOfCreation)
go