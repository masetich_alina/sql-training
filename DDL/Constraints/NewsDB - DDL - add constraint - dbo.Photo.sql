use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Photo__Author'
)
	alter table dbo.Photo
	drop constraint fk_Photo__Author
go

alter table dbo.Photo
	add constraint fk_Photo__Author
	foreign key (authorId) references dbo.Author(authorId)
	on delete cascade
go

