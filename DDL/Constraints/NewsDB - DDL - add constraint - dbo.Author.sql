use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Author__Person'
)
	alter table dbo.Author
	drop constraint fk_Author__Person
go

alter table dbo.Author
	add constraint fk_Author__Person
	foreign key (personId) references dbo.Person(personId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Author__PersonId'
)
	alter table dbo.Author
	drop constraint ak_Author__PersonId
go

alter table dbo.Author
add constraint ak_Author__PersonId unique (personId)
go
