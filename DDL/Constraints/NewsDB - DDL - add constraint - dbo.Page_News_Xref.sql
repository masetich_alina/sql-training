use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Page_News_Xref__Page'
)
	alter table dbo.Page_News_Xref
	drop constraint fk_Page_News_Xref__Page
go

alter table dbo.Page_News_Xref
	add constraint fk_Page_News_Xref__Page
	foreign key (pageId) references dbo.[Page](pageId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Page_News_Xref__News'
)
	alter table dbo.Page_News_Xref
	drop constraint fk_Page_News_Xref__News
go

alter table dbo.Page_News_Xref
	add constraint fk_Page_News_Xref__News
	foreign key (newsId) references dbo.News(newsId)
	on delete no action
go
