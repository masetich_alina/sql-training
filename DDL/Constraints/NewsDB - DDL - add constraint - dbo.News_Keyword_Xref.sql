use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News_Keyword_Xref__News'
)
	alter table dbo.News_Keyword_Xref
	drop constraint fk_News_Keyword_Xref__News
go

alter table dbo.News_Keyword_Xref
	add constraint fk_News_Keyword_Xref__News
	foreign key (newsId) references dbo.News(newsId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News_Keyword_Xref__Keyword'
)
	alter table dbo.News_Keyword_Xref
	drop constraint fk_News_Keyword_Xref__Keyword
go

alter table dbo.News_Keyword_Xref
	add constraint fk_News_Keyword_Xref__Keyword
	foreign key (keywordId) references dbo.Keyword(keywordId)
	on delete cascade
go