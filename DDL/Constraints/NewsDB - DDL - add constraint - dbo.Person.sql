use News
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Person__Email'
)
	alter table dbo.Person
	drop constraint ak_Person__Email
go

alter table dbo.Person
add constraint ak_Person__Email unique (email)
go