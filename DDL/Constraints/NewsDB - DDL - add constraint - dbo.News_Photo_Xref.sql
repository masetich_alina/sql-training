use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News_Photo_Xref__News'
)
	alter table dbo.News_Photo_Xref
	drop constraint fk_News_Photo_Xref__News
go

alter table dbo.News_Photo_Xref
	add constraint fk_News_Photo_Xref__News
	foreign key (newsId) references dbo.News(newsId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News_Photo_Xref__Photo'
)
	alter table dbo.News_Photo_Xref
	drop constraint fk_News_Photo_Xref__Photo
go

alter table dbo.News_Photo_Xref
	add constraint fk_News_Photo_Xref__Photo
	foreign key (photoId) references dbo.Photo(photoId)
	on delete cascade
go