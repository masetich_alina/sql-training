use News
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Keyword__NameKeyword'
)
	alter table dbo.Keyword
	drop constraint ak_Keyword__NameKeyword
	
go

alter table dbo.Keyword
add constraint ak_Keyword__NameKeyword unique (nameKeyword)
go