use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Subcategory__Category'
)
	alter table dbo.Subcategory
	drop constraint fk_Subcategory__Category
go

alter table dbo.Subcategory
	add constraint fk_Subcategory__Category
	foreign key (categoryId) references dbo.Category(categoryId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Subcategory__CategoryId__Name'
)
	alter table dbo.Subcategory
	drop constraint ak_Subcategory__CategoryId__Name
go

alter table dbo.Subcategory
add constraint ak_Subcategory__CategoryId__Name unique (categoryId, [name])
go