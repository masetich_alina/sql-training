use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Category_Page_News_Xref__Page'
)
	alter table dbo.Category_Page_News_Xref
	drop constraint fk_Category_Page_News_Xref__Page
go

alter table dbo.Category_Page_News_Xref
	add constraint fk_Category_Page_News_Xref__Page
	foreign key (pageId) references dbo.[Page](pageId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Category_Page_News_Xref__Category'
)
	alter table dbo.Category_Page_News_Xref
	drop constraint fk_Category_Page_News_Xref__Category
go

alter table dbo.Category_Page_News_Xref
	add constraint fk_Category_Page_News_Xref__Category
	foreign key (categoryId) references dbo.Category(categoryId)
	on delete no action
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_Category_Page_News_Xref__News'
)
	alter table dbo.Category_Page_News_Xref
	drop constraint fk_Category_Page_News_Xref__News
go

alter table dbo.Category_Page_News_Xref
	add constraint fk_Category_Page_News_Xref__News
	foreign key (newsId) references dbo.News(newsId)
	on delete no action
go
