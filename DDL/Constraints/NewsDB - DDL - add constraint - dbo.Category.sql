use News
go

if exists
(
	select 1 
	from information_schema.constraint_column_usage
	where Constraint_Name ='ak_Category__Name'
)
	alter table dbo.Category
	drop constraint ak_Category__Name
go

alter table dbo.Category
add constraint ak_Category__Name unique ([name])
go