use News
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News_Video_Xref__News'
)
	alter table dbo.News_Video_Xref
	drop constraint fk_News_Video_Xref__News
go

alter table dbo.News_Video_Xref
	add constraint fk_News_Video_Xref__News
	foreign key (newsId) references dbo.News(newsId)
	on delete cascade
go

if exists
(
	select 1 
	from information_schema.referential_constraints
	where Constraint_Name ='fk_News_Video_Xref__Video'
)
	alter table dbo.News_Video_Xref
	drop constraint fk_News_Video_Xref__Video
go

alter table dbo.News_Video_Xref
	add constraint fk_News_Video_Xref__Video
	foreign key (videoId) references dbo.Video(videoId)
	on delete cascade
go