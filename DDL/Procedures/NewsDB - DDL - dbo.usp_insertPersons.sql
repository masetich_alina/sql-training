use News

if exists (select * from sys.procedures where Name = N'usp_insertPersons')
begin
    drop procedure dbo.usp_insertPersons;
end
go

create procedure dbo.usp_insertPersons
    @json nvarchar(MAX)
as
    declare @tempTable table(
        rowId               int                 not null        identity primary key,
        email               nvarchar(40)        not null,
        [password]          nvarchar(40)        not null,
        dateOfRegister      datetime            not null,
        author              nvarchar(max),
        personInfo          nvarchar(max)
    )
    
    insert into @tempTable (email, [password], dateOfRegister, author, personInfo)				
    select j.*						
    from openjson(@json)		
        with(
        email               nvarchar(40)        N'$.email',	
        [password]          nvarchar(40)        N'$.password',
        dateOfRegister      datetime            N'$.dateOfRegister',
        author              nvarchar(max)       N'$.author'				as json,
        personInfo          nvarchar(max)       N'$.personInfo'			as json
        ) as j
    
    declare	@amount int
    select	@amount = @@rowcount
    declare	@counter int = 1	
    
    while (@counter <= @amount)
    begin
        declare	@email              nvarchar(40),
        		@password           nvarchar(40),
        		@dateOfRegister     datetime,		
        		@author             nvarchar(max),
        		@personInfo         nvarchar(max),
        		@personId           int
        
        select	@email = email,
        		@password = [password],
        		@dateOfRegister = dateOfRegister,
        		@author = author,
        		@personInfo = personInfo
        from @tempTable
        where rowId = @counter
        
        insert into dbo.Person (email, [password], dateOfRegister)
        values (@email, @password, @dateOfRegister)
        
        set @personId = scope_identity()
        
        insert into dbo.PersonInfo(personId, [date], firstName, lastname, dateOfBirthday, city, occupation, interests)
        select @personId, j.[date], j.firstName, j.lastName, j.dateOfBirthday, j.city, j.occupation, j.interests
        from openjson(@personInfo)
            with(
                firstName           nvarchar(40)        N'$.firstName',
                lastName            nvarchar(40)        N'$.lastName',
                [date]              datetime            N'$.date',
                dateOfBirthday      date                N'$.dateOfBirthday',
                city                nvarchar(40)        N'$.city',
                occupation          nvarchar(40)        N'$.occupation',
                interests           nvarchar(150)       N'$.interests'            
            ) as j
        
        insert into dbo.Author(personId, info)
        select @personId, j.info
        from openjson(@author)
        	with(info	nvarchar(100)	N'$.info') as j
        	
        set @counter = @counter + 1
    end	
go
