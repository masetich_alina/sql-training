use News
go

if exists (select * from sys.procedures where Name = N'usp_insertNews')
begin
    drop procedure dbo.usp_insertNews;
end
go

create procedure dbo.usp_insertNews
	@json nvarchar(max) = null
as
    declare @tempTable table(
        rowId                   int                 not null        identity primary key,
        author                  nvarchar(40)        not null,
        miniHeader              nvarchar(150),
        header                  nvarchar(250)       not null,
        summary                 nvarchar(1000),
        fullContent             nvarchar(max)       not null,
        publicationDate         datetime            not null,
        comment                 nvarchar(max),
        photo                   nvarchar(max),
        video                   nvarchar(max),
        keyword                 nvarchar(max),
        category                nvarchar(max)       not null		
    )
    
    insert into @tempTable (author,					
                            miniHeader,	
                            header,			
                            summary,			
                            fullContent	,
                            publicationDate	,
                            comment,			
                            photo,			
                            video,			
                            keyword	,		
                            category)				
    select j.*						
    from openjson(@json)		
    	with(
    	author                  nvarchar(40)        N'$.author',
    	miniHeader              nvarchar(150)       N'$.miniHeader',
    	header                  nvarchar(250)       N'$.header',
    	summary                 nvarchar(1000)      N'$.summary',
    	fullContent             nvarchar(max)       N'$.fullContent',
    	publicationDate         datetime            N'$.publicationDate',
    	comment                 nvarchar(max)       N'$.comment'            as json,
    	photo                   nvarchar(max)       N'$.photo'              as json,
    	video                   nvarchar(max)       N'$.video'              as json,
    	keyword                 nvarchar(max)       N'$.keyword'            as json,
    	category                nvarchar(max)       N'$.category'           as json
    	) 
    	as j
    
    declare	@amount int
    select	@amount = @@rowcount
    declare	@counter int = 1	
    
    while (@counter <= @amount)
    begin
        declare	@author                 nvarchar(40),	
        		@miniHeader             nvarchar(150),	
        		@header                 nvarchar(250),	
        		@summary                nvarchar(1000),	
        		@fullContent            nvarchar(max),	
        		@publicationDate        datetime,	
        		@comment                nvarchar(max),	
        		@photo                  nvarchar(max),	
        		@video                  nvarchar(max),	
        		@keyword                nvarchar(max),	
        		@category               nvarchar(max),
        		@newsId                 int,
        		@commentId              int
        
        select	
        	@author	= author,	
        	@miniHeader = miniHeader,	
        	@header = header,	
        	@summary = summary,
        	@fullContent = fullContent,	
        	@publicationDate = publicationDate,
        	@comment = comment,
        	@photo = photo,			
        	@video = video,	
        	@keyword = keyword,
        	@category =	category		
        
        from @tempTable
        where rowId = @counter
        
        declare @authorId int
        select @authorId = authorId from dbo.Author 
        join dbo.Person p on p.email = @author
        
        insert into dbo.News(authorId, miniHeader, header, summary, fullContent, publicationDate)
        values(@authorId, @miniHeader, @header, @summary, @fullContent, @publicationDate)
        
        set @newsId = scope_identity()
        declare @like           nvarchar(max),	
        		@dislike        nvarchar(max)        
        
        insert into dbo.Comment(personId, newsId, [text], dateOfCreation)
        select p.personId, @newsId, j.[text], j.dateOfCreation
        from openjson(@comment)
            with(
        	    person              nvarchar(40)        N'$.person',
        	    dateOfCreation      datetime            N'$.dateOfCreation',
        	    [text]              nvarchar(150)       N'$.text'
        	    ) as j
        	join dbo.Person p on p.email = j.person
        
        select	@like = j.[like],
        		@dislike = j.dislike
        from openjson(@comment)
            with(
        	    [like]          nvarchar(max)       N'$.like'           as json,
        	    dislike         nvarchar(max)       N'$.dislike'        as json
        	    ) as j
        
        set @commentId = IDENT_CURRENT('Comment')
        insert into dbo.Like_Comment_Person_Xref(commentId, personId)
        select @commentId, p.personId
        from openjson(@like)
            with(person      nvarchar(40)        N'$') as j
        	join dbo.Person p on p.email = j.person
        
        insert into dbo.Dislike_Comment_Person_Xref(commentId, personId)
        select @commentId, p.personId
        from openjson(@dislike)
            with(person      nvarchar(40)        N'$') as j
        	join dbo.Person p on p.email = j.person
        	
        insert into dbo.News_Category_Xref(newsId, categoryId)
        select @newsId, c.categoryId
        from openjson(@category)
            with(
        	    nameCategory        nvarchar(40)    N'$.name',
        	    subcategory         nvarchar(max)   N'$.subcategory'    as json
        	    ) as j
        	join dbo.Category c on c.[name] = j.nameCategory
        
        insert into dbo.News_Keyword_Xref(newsId, keywordId)
        select @newsId, k.keywordId
        from openjson(@keyword)
            with(nameKeyword     nvarchar(40)        N'$') as j
            join dbo.Keyword k on k.nameKeyword = j.nameKeyword
        
        set @counter = @counter + 1
    end
go
