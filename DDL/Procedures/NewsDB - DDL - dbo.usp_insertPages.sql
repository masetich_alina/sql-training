use News
go

if exists (select * from sys.procedures where Name = N'usp_insertPages')
begin
    drop procedure dbo.usp_insertPages;
end
go

create procedure dbo.usp_insertPages
    @json nvarchar(max) = null
as
    declare @tempTable table(
        rowId           int                 not null        identity primary key,
        [date]          datetime            not null,
        mainNews        nvarchar(max)       not null,
        rubric          nvarchar(max)       not null    	
    )
    
    insert into @tempTable ([date], mainNews, rubric)				
    select j.*						
    from openjson(@json)		
    	with(
    	[date]          datetime            N'$.date',	
    	mainNews        nvarchar(max)       N'$.mainNews'       as json,
    	rubric          nvarchar(max)       N'$.rubric'         as json		
    	) as j
    
    declare	@amount int
    select	@amount = @@rowcount
    declare	@counter int = 1	
    
    while (@counter <= @amount)
    begin
        declare	@date           datetime,		
        		@mainNews       nvarchar(max),
        		@rubric         nvarchar(max),
        		@pageId         int
        
        select	@date = [date],
        		@mainNews = mainNews,
        		@rubric = rubric				
        from @tempTable
        where rowId = @counter
        
        insert into dbo.[Page] ([date])
        values (@date)
        
        set @pageId = scope_identity()
        
        insert into dbo.Page_News_Xref(pageId, newsId)
        select @pageId, n.newsId
        from openjson(@mainNews)
        	with(news	nvarchar(max)	N'$'	as json)
        	outer apply openjson(news) 
        		with( 
        			author              nvarchar(40)    '$.author',
        			publicationDate     nvarchar(40)    '$.publicationDate'
        			) as j
        		join dbo.Person     p on p.email            = j.author
        		join dbo.Author     a on a.personId         = p.personId
        		join dbo.News       n on n.publicationDate  = j.publicationDate and n.authorId = a.authorId
        
        insert into dbo.Category_Page_News_Xref(pageId, categoryId, newsId)
        select @pageId, c.categoryId, n.newsId
        from openjson(@rubric)
            with(
            	category        nvarchar(20)        N'$.category',
            	newsInfo        nvarchar(max)       N'$.newsInfo'       as json
                ) as r
            join dbo.Category c ON c.[name] = r.category
            outer apply openjson(newsInfo) 
            	with( 
            		author              nvarchar(40)    '$.author',
            		publicationDate     nvarchar(40)    '$.publicationDate'
            		) as j
            	join dbo.Person     p on p.email            = j.author
            	join dbo.Author     a on a.personId         = p.personId
            	join dbo.News       n on n.publicationDate  = j.publicationDate and n.authorId = a.authorId
        
        set @counter = @counter + 1
    end
go

