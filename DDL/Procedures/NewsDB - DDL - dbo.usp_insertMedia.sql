use News
go

if exists (select * from sys.procedures where Name = N'usp_insertMedia')
begin
    drop procedure dbo.usp_insertMedia;
end
go

create procedure dbo.usp_insertMedia
    @json nvarchar(max) = null
as
    declare @photoTempTable table(
        rowId                           int                 not null    identity primary key,
        img                             nvarchar(max),	
        author                          nvarchar(40),	
        header                          bit,
        miniHeader                      bit,
        mainNews                        bit,
        mainNewsInCategory              bit,
        secondMainNewsInCategory        bit,
        headerInCategory                bit,
        miniHeaderInCategory            bit,
        newsInfo                        nvarchar(max)
        )
    
    insert into @photoTempTable(img,						
                                author,					
                                header,					
                                miniHeader,				
                                mainNews,				
                                mainNewsInCategory,		
                                secondMainNewsInCategory,
                                headerInCategory,		
                                miniHeaderInCategory,	
                                newsInfo
                                )				
    select j.*						
    from openjson(JSON_QUERY(@json, '$.photo'))		
        with(
            img                             nvarchar(max)       N'$.img',
            author                          nvarchar(40)        N'$.author',
            header                          bit                 N'$.header',
            miniHeader                      bit                 N'$.miniHeader',
            mainNews                        bit                 N'$.mainNews',
            mainNewsInCategory              bit                 N'$.mainNewsInCategory',
            secondMainNewsInCategory        bit                 N'$.secondMainNewsInCategory',
            headerInCategory                bit                 N'$.headerInCategory',
            miniHeaderInCategory            bit                 N'$.miniHeaderInCategory',
            newsInfo                        nvarchar(max)       N'$.newsInfo'       as json
            ) as j
    
    declare	@amount int
    select	@amount = @@rowcount
    declare	@counter int = 1	
    
    while (@counter <= @amount)
    begin
    	declare  @img                           nvarchar(max),
    			 @authorPhoto                   nvarchar(40),
    			 @header                        bit,
    			 @miniHeader                    bit,
    			 @mainNews                      bit,
    			 @mainNewsInCategory            bit,
    			 @secondMainNewsInCategory      bit,
    			 @headerInCategory              bit,
    			 @miniHeaderInCategory          bit,
    			 @newsInfoPhoto                 nvarchar(max),
    			 @photoId                       int
    	select
    		@img = img,
    		@authorPhoto = author,
    		@header = header,
    		@miniHeader = miniHeader,
    		@mainNews = mainNews,
    		@mainNewsInCategory = mainNewsInCategory,
    		@secondMainNewsInCategory = secondMainNewsInCategory,
    		@headerInCategory = headerInCategory,
    		@miniHeaderInCategory = miniHeaderInCategory,
    		@newsInfoPhoto = newsInfo
    	from @photoTempTable
    	where rowId = @counter
    
    	declare @authorPhotoId int
    	select @authorPhotoId = authorId 
        from dbo.Author 
    	    join dbo.Person p on p.email = @authorPhoto
    
    	insert into dbo.Photo(authorId, img, header, miniHeader, mainNews, mainNewsInCategory, secondMainNewsInCategory, headerInCategory, miniHeaderInCategory)
    	values(@authorPhotoId, @img, @header, @miniHeader, @mainNews, @mainNewsInCategory, @secondMainNewsInCategory, @headerInCategory, @miniHeaderInCategory)
    
    	insert into dbo.News_Photo_Xref(newsId, photoId)
    	select n.newsId, SCOPE_IDENTITY()
    	from openjson(@newsInfoPhoto)
    	    with(
                publicationDate     datetime        N'$.publicationDate',
                author              nvarchar(40)    N'$.author'
    	        ) as j
    	    join dbo.Person     p on p.email            = j.author
    	    join dbo.Author     a on a.personId         = p.personId
    	    join dbo.News       n on n.publicationDate  = j.publicationDate and n.authorId = a.authorId
    
    	set @counter = @counter + 1
    end
    declare @videoTempTable table(
        rowIdVideo          int                 not null        identity primary key,
        video               nvarchar(max),	
        authorVideo         nvarchar(40),	
        newsInfo            nvarchar(max)
    	)
    
    insert into @videoTempTable (video,						
    							authorVideo,		
    							newsInfo
    							)				
    select j.*						
    from openjson(JSON_QUERY(@json, '$.video'))		
    	with(
    		video           nvarchar(max)       N'$.video',
    		authorVideo     nvarchar(40)        N'$.author',
    		newsInfo        nvarchar(max)       N'$.newsInfo'       as json
    	    ) as j
    
    declare	@amountVideo int
    select	@amountVideo = @@rowcount
    declare	@counterVideo int = 1	
    while (@counterVideo <= @amountVideo)
    begin
        declare  @video             nvarchar(max),
        		 @authorVideo       nvarchar(40),
        		 @newsInfoVideo     nvarchar(max),
        		 @videoId           int
        select
        	@video = video,					
        	@authorVideo = authorVideo,		
        	@newsInfoVideo = newsInfo	
        from @videoTempTable
        where rowIdVideo = @counterVideo
        
        declare @authorVideoId int
        select @authorVideoId = authorId 
        from dbo.Author 
            join dbo.Person p on p.email = @authorVideo
        
        insert into dbo.Video(authorId, video)
        values(@authorVideoId, @video)
        
        insert into dbo.News_Video_Xref(newsId, videoId)
        select n.newsId, SCOPE_IDENTITY()
        from openjson(@newsInfoVideo)
            with(
            	publicationDate     datetime        N'$.publicationDate',
            	author	            nvarchar(40)    N'$.author'
                ) as j
            join dbo.Person     p on p.email            = j.author
            join dbo.Author     a on a.personId         = p.personId
            join dbo.News       n on n.publicationDate  = j.publicationDate and n.authorId = a.authorId
        
        set @counterVideo = @counterVideo + 1
    end
go
