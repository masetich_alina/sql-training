use News
go

if exists (select * from sys.procedures where Name = N'usp_insert_Category_Keyword')
begin
    drop procedure dbo.usp_insert_Category_Keyword;
end
go

create procedure dbo.usp_insert_Category_Keyword
    @json nvarchar(max) = null
as
    insert into dbo.Keyword(nameKeyword)				
    select j.[name]
        from openjson(JSON_QUERY(@json, '$.keyword'))
            with ([name]      nvarchar(40)     '$') as j
    
    declare @tempTable table(
        rowId           int                 not null        identity primary key,
        [name]          nvarchar(40)        not null,
        subcategory     nvarchar(max)
    )
    
    insert into @tempTable ([name], subcategory)				
    select j.*
        from openjson(JSON_QUERY(@json, '$.category'))
            with ( 
    	        [name]          nvarchar(40)        N'$.name',
    	        subcategory     nvarchar(max)       N'$.subcategory'    as json
    	    ) as j
    
    declare	@amount int
    select	@amount = @@rowcount
    declare	@counter int = 1	
    
    while (@counter <= @amount)
    begin
    	declare	@name           nvarchar(40),
    			@subcategory    nvarchar(max),
    			@categoryId     int
    
    	select	@name = [name],
    	        @subcategory = subcategory
    	from @tempTable
    	where rowId = @counter
    
    	insert into dbo.Category([name])
    	values(@name)
    	
    	set @categoryId = SCOPE_IDENTITY()
    
    	insert into dbo.Subcategory(categoryId, [name])
    	select @categoryId, j.[name]
    	from openjson(@subcategory)
    	    with([name]     nvarchar(40)    '$') as j
    
    	set @counter = @counter + 1
    end
go

