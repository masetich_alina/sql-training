use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'News_Video_Xref'
)
	drop table dbo.News_Video_Xref
	go

create table dbo.News_Video_Xref
(
	newsId			int		not null,
	videoId			int		not null,

	constraint pk_News_Video_Xref primary key (newsId, videoId)
)
go