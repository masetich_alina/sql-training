use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Keyword'
)
	drop table dbo.Keyword
	go

create table dbo.Keyword
(
	keywordId			int				not null	identity,
	nameKeyword			nvarchar(40)	not null,

	constraint pk_Keyword primary key (keywordId)
)
go