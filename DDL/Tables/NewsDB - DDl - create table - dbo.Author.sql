use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Author'
)
	drop table dbo.Author
	go

create table dbo.Author
(
	authorId		int              not null   identity,
	personId		int				 not null,
	info			nvarchar(100),    
			
	constraint pk_Author primary key (authorId)
)
go