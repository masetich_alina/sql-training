use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'News_Keyword_Xref'
)
	drop table dbo.News_Keyword_Xref
	go

create table dbo.News_Keyword_Xref
(
	newsId			int		not null,
	keywordId		int		not null,

	constraint pk_News_Keyword_Xref primary key (newsId, keywordId)
)
go