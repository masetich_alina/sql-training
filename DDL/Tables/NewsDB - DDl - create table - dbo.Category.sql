use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Category'
)
	drop table dbo.Category
	go

create table dbo.Category
(
	categoryId		tinyint			not null	identity,
	[name]			nvarchar(40)	not null,

	constraint pk_Category primary key (categoryId)
)
go