use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Category_Page_News_Xref'
)
	drop table dbo.Category_Page_News_Xref
	go

create table dbo.Category_Page_News_Xref
(
	pageId				int				not null,
	categoryId			tinyint			not null,
	newsId				int				not null,
	
	constraint pk_Category_Page_News_Xref primary key (pageId, categoryId, newsId)
)
go