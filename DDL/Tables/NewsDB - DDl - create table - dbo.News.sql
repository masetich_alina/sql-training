use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'News'
)
	drop table dbo.News
	go

create table dbo.News
(
	newsId				int             not null   identity,
	authorId			int				not null,
	miniHeader			nvarchar(150),
	header				nvarchar(250)	not null,
	summary				nvarchar(1000),
	fullContent			nvarchar(max)	not null,
	publicationDate		datetime		not null,		

	constraint pk_News primary key (newsId)
)
go