use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Video'
)
	drop table dbo.Video
	go

create table dbo.Video
(
	videoId			int					not null   identity,
	authorId		int					not null,
	video			nvarchar(max)	    not null,

	constraint pk_Video primary key (videoId)
)
go