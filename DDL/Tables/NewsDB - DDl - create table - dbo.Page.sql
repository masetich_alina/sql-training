use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Page'
)
	drop table dbo.[Page]
	go

create table dbo.[Page]
(
	pageId		int			not null   identity,
	[date]		datetime	not null,
		
	constraint pk_Page primary key (pageId)
)
go