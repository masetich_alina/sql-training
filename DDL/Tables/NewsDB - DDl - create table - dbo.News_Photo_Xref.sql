use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'News_Photo_Xref'
)
	drop table dbo.News_Photo_Xref
	go

create table dbo.News_Photo_Xref
(
	newsId			int,
	photoId			int,

	constraint pk_News_Photo_Xref primary key (newsId, photoId)
)
go