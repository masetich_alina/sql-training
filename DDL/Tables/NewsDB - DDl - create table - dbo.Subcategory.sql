use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Subcategory'
)
	drop table dbo.Subcategory
	go

create table dbo.Subcategory
(
	subcategoryId			tinyint			not null	identity,
	categoryId				tinyint			not null,
	[name]			 		nvarchar(40)	not null,

	constraint pk_Subcategory primary key (subcategoryId)
)
go