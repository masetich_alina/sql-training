use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Page_News_Xref'
)
	drop table dbo.Page_News_Xref
	go

create table dbo.Page_News_Xref
(
	pageId			int			not null,
	newsId			int			not null,
	
	constraint pk_Page_News_Xref primary key (pageId, newsId)
)
go