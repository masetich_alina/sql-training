use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Person'
)
	drop table dbo.Person
	go

create table dbo.Person
(
	personId			int             not null   identity,
	email				nvarchar(40)    not null,
	[password]			nvarchar(40)    not null,
	dateOfRegister      datetime,

	constraint pk_Person primary key (personId)
)
go