use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Dislike_Comment_Person_Xref'
)
	drop table dbo.Dislike_Comment_Person_Xref
	go

create table dbo.Dislike_Comment_Person_Xref
(
	commentId		int		not null,
	personId		int		not null,

	constraint pk_Dislike_Comment_Person_Xref primary key (commentId, personId)
)
go