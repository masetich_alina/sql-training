use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'PersonInfo'
)
	drop table dbo.PersonInfo
	go

create table dbo.PersonInfo
(
	personInfoId	int             not null   identity,
	personId		int				not null,
	[date]			datetime		not null,
	firstname		nvarchar(40),    
	lastname		nvarchar(40),	
	city			nvarchar(40),	
	occupation		nvarchar(40),	
	interests		nvarchar(150),	
	dateOfBirthday	date,	

	constraint pk_PersonInfo primary key (personInfoId)
)
go