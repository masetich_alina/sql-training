use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Photo'
)
	drop table dbo.Photo
	go

create table dbo.Photo
(
	photoId						int					not null   identity,
	authorId					int					not null,
	img							nvarchar(max)		not null,
	header						bit,
	miniHeader					bit,
	mainNews					bit,
	mainNewsInCategory			bit,
	secondMainNewsInCategory	bit,
	headerInCategory			bit,
	miniHeaderInCategory		bit,

	constraint pk_Photo primary key (photoId),
)
go