use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'News_Category_Xref'
)
	drop table dbo.News_Category_Xref
	go

create table dbo.News_Category_Xref
(
	newsId			int			not null,
	categoryId		tinyint		not null,

	constraint pk_News_Category_Xref primary key (newsId, categoryId)
)
go