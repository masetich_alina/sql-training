use News
go

if exists
(
	select 1 
	from sys.Tables
	where Name = 'Comment'
)
	drop table dbo.Comment
	go

create table dbo.Comment
(
	commentId			int			    not null		identity,
	personId			int				not null,
	newsId				int				not null,
	[text]				nvarchar(150)	not null,
	dateOfCreation		datetime,		

	constraint pk_Comment primary key (commentId)
)
go